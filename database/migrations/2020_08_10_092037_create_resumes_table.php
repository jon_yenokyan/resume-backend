<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('first_name');
            $table->string('last_name');
            $table->text('img_path')->nullable();
            $table->string('city');
            $table->text('address');
            $table->text('phone');
            $table->enum('armenian', [0 , 1 , 2 , 3 , 4 , 5]);
            $table->enum('russian', [0 , 1 , 2 , 3 , 4 , 5]);
            $table->enum('english', [0 , 1 , 2 , 3 , 4 , 5]);
            $table->text('description')->nullable();
            $table->text('fb_link')->nullable();
            $table->text('linkedin_link')->nullable();
            $table->text('telegram')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
