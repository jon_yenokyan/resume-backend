<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $fillable = ['name'];
    public function resume(){
        $this->belongsTo(Resume::class , 'resume_id' , 'id');
    }
}
