<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ResumeResource;
use App\Resume;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResumeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resume = Resume::with('skills' , 'experience' , 'education')->where('user_id' , Auth::id())->get();
        return ResumeResource::collection( $resume );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validateResume= $request->validate([
            'first_name'                => 'required|max:255',
            'last_name'                 => 'required|max:255',
            'city'                      => 'required|max:255',
            'address'                   => 'required',
            'phone'                    => 'required|max:19|min:8',
            'armenian'                  => 'in:0,1,2,3,4,5',
            'russian'                   => 'in:0,1,2,3,4,5',
            'english'                   => 'in:0,1,2,3,4,5',
            "skills"                    => "required|array|min:1|max:10",
            "skills.*.name"              => "required|max:255",
            "experiences"               => "required|array|min:1|max:10",
            "experiences.*.name"         => "required|max:255",
            "experiences.*.position"     => "required|max:255",
            "experiences.*.from_date"    => "required|date_format:Y-m-d",
            "educations"                => "required|array|min:1|max:10",
            "education.*.name"           => "required|max:255",
            "education.*.fakultet"       => "required|max:255",
            "education.*.address"        => "required|max:255",
            "education.*.from_date"      => "required|date_format:Y-m-d",
        ]);

        $resume = new Resume($request->all());

         if ($request->hasFile('file')) {
                $image = $request->file('file');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/resume_images');
                $image->move($destinationPath, $name);
            $resume->img_path = '/resume_images/'.$name;
         }
            $resume->user_id  = Auth::id();
        $resume->save();

        $skills = [];

        foreach($request->skills as $item){
            $resume->skills()->create($item);
        }

        foreach($request->educations as $item){
            $resume->education()->create($item);
        }

        foreach($request->experiences as $item){
            $resume->experience()->create($item);
        }

        return  response(['message' => 'Resume Created']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resume = Resume::with('skills' , 'experience' , 'education')->where('user_id' , Auth::id())->find($id);
        return new ResumeResource($resume);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //todo
        /*$validateResume= $request->validate([
            'first_name'                => 'required|max:255',
            'last_name'                 => 'required|max:255',
            'img_path'                  => 'required',
            'city'                      => 'required|max:255',
            'address'                   => 'required',
            'number'                    => 'required|max:19|min:8',
            'armenian'                  => 'in:0,1,2,3,4,5',
            'russian'                   => 'in:0,1,2,3,4,5',
            'english'                   => 'in:0,1,2,3,4,5',
            "skills"                    => "required|array|min:1|max:10",
            "skills.*name"              => "required|max:255",
            "skills.*status"            => "in:0,1,2,3,4,5",
            "experiences"               => "required|array|min:1|max:10",
            "experiences.*name"         => "required|max:255",
            "experiences.*position"     => "required|max:255",
            "experiences.*from_date"    => "required|date_format:d/m/Y",
            "educations"                => "required|array|min:1|max:10",
            "education.*name"           => "required|max:255",
            "education.*fakultet"       => "required|max:255",
            "education.*address"        => "required|max:255",
            "education.*from_date"      => "required|date_format:d/m/Y",
        ]);

        $resume = new Resume($id);
        $resume->first_name         = $request->first_name;
        $resume->last_name          = $request->last_name;
        $resume->img_path           = $request->img_path;
        $resume->city               = $request->city;
        $resume->address            = $request->address;
        $resume->phone              = $request->phone;
        $resume->armenian           = $request->armenian;
        $resume->russian            = $request->russian;
        $resume->english            = $request->english;
        $resume->description        = $request->description;
        $resume->fb_link            = $request->fb_link;
        $resume->linkedin_link      = $request->linkedin_link;
        $resume->telegram           = $request->telegram;
        $resume->save();

        $resume->skills()->create($request->skills);
        $resume->education()->create($request->educations);
        $resume->experience()->create($request->experiences);
        */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resume $resume)
    {
        $resume->delete();
        return response(['message' => 'Resume deleted']);
    }
}
