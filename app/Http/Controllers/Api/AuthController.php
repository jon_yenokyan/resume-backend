<?php

namespace App\Http\Controllers\Api;

use App\Jobs\CreatedUserEmailJob;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        $validatedData['password'] = bcrypt($request->password);
        $user = User::create($validatedData);

        $access_token = $user->createToken('authToken')->accessToken;

        dispatch(new CreatedUserEmailJob($user));
        return response(['user' => $user, 'access_token' => $access_token]);
    }


    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required',
        ]);

        if(!auth()->attempt($loginData)){
            return response([
                    'message' => 'Invalid authantication'
                ], 422
            );
        }

        $access_token = auth()->user()->createToken('authToken')->accessToken;
        return response(['user' => auth()->user(), 'access_token' => $access_token]);

    }
}
