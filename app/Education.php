<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $fillable = [
        'name',
        'fakultet',
        'address',
        'from_date',
        'to_date',
    ];
    public function resume(){
        $this->belongsTo(Resume::class , 'resume_id' , 'id');
    }
}
