<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = [
        'name',
        'position',
        'address',
        'from_date',
        'to_date',
    ];
    public function resume(){
        $this->belongsTo(Resume::class , 'resume_id' , 'id');
    }
}
