<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    protected $fillable = [
                       'first_name',
                       'last_name',
                       'img_path',
                       'city',
                       'address',
                       'phone',
                       'armenian',
                       'russian',
                       'english',
                       'description',
                       'fb_link',
                       'linkedin_link',
                       'telegram'];
    public function user(){
        $this->belongsTo('\App\User' , 'user_id' , 'id');
    }

    public function skills(){
        return $this->hasMany(Skill::class);
    }

    public function experience(){
        return $this->hasMany(Experience::class);
    }
    public function education(){
        return $this->hasMany(Education::class);
    }
}
